<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Grouping", mappedBy="users")
     */
    private $groupings;

    public function __construct()
    {
        parent::__construct();
        $this->groupings = new ArrayCollection();
    }

    /**
     * @return Collection|Grouping[]
     */
    public function getGroupings(): Collection
    {
        return $this->groupings;
    }

    public function addGrouping(Grouping $grouping): self
    {
        if (!$this->groupings->contains($grouping)) {
            $this->groupings[] = $grouping;
            $grouping->addUser($this);
        }

        return $this;
    }

    public function removeGrouping(Grouping $grouping): self
    {
        if ($this->groupings->contains($grouping)) {
            $this->groupings->removeElement($grouping);
            $grouping->removeUser($this);
        }

        return $this;
    }

}
