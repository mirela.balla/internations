<?php
namespace App\Rest\Controller;

use App\Repository\GroupingRepository;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

class GroupingController extends FOSRestController
{
    /**
     * @var \App\Repository\GroupingRepository
     */
    private $groupingRepository;

    public function __construct(GroupingRepository $groupingRepository)
    {
        $this->groupingRepository     = $groupingRepository;
    }

    /**
     * Retrieves an Group resource
     * @Rest\Get("/grouping/{groupingId}")
     */
    public function getGrouping(int $groupingId): View
    {
        $grouping = $this->groupingRepository->findById($groupingId);

        // In case our GET was a success we need to return a 200 HTTP OK response with the request object
        return View::create($grouping, Response::HTTP_OK);
    }
}