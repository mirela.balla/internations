<?php

namespace App\Controller;

use App\Entity\Grouping;
use App\Form\GroupingType;
use App\Repository\GroupingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route("/grouping")
 */
class GroupingController extends AbstractController
{
    /**
     * @Route("/", name="grouping_index", methods={"GET"})
     */
    public function index(GroupingRepository $groupingRepository, AuthorizationCheckerInterface $authChecker): Response
    {
        if (false === $authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException('Unable to access this page!');
        }
        return $this->render('grouping/index.html.twig', [
            'groupings' => $groupingRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="grouping_new", methods={"GET","POST"})
     */
    public function new(Request $request, AuthorizationCheckerInterface $authChecker): Response
    {
        if (false === $authChecker->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException('Unable to access this page!');
        }

        $grouping = new Grouping();
        $form = $this->createForm(GroupingType::class, $grouping);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($grouping);
            $entityManager->flush();

            return $this->redirectToRoute('grouping_index');
        }

        return $this->render('grouping/new.html.twig', [
            'grouping' => $grouping,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="grouping_show", methods={"GET"})
     */
    public function show(Grouping $grouping, AuthorizationCheckerInterface $authChecker): Response
    {
        if (false === $authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException('Unable to access this page!');
        }

        return $this->render('grouping/show.html.twig', [
            'grouping' => $grouping,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="grouping_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Grouping $grouping, AuthorizationCheckerInterface $authChecker): Response
    {
        if (false === $authChecker->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException('Unable to access this page!');
        }

        $form = $this->createForm(GroupingType::class, $grouping);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('grouping_index', [
                'id' => $grouping->getId(),
            ]);
        }

        return $this->render('grouping/edit.html.twig', [
            'grouping' => $grouping,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="grouping_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Grouping $grouping, AuthorizationCheckerInterface $authChecker): Response
    {
        if (false === $authChecker->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException('Unable to access this page!');
        }

        if ($this->isCsrfTokenValid('delete'.$grouping->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();

            if(0 === $grouping->getUsers()->count()){
                $entityManager->remove($grouping);
                $entityManager->flush();
            }else{

                // Add flash message to notify group was not deleted
            }
        }

        return $this->redirectToRoute('grouping_index');
    }
}
