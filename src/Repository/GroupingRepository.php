<?php

namespace App\Repository;

use App\Entity\Grouping;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Grouping|null find($id, $lockMode = null, $lockVersion = null)
 * @method Grouping|null findOneBy(array $criteria, array $orderBy = null)
 * @method Grouping[]    findAll()
 * @method Grouping[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Grouping::class);
    }

    // /**
    //  * @return Grouping[] Returns an array of Grouping objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Grouping
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
